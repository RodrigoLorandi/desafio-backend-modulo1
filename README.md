# DESAFIO DO MÓDULO 1 #

## Descrição ##
Nesta aplicação foi feito um sistema para gerenciamento de uma loja virtual. 

Com ela é possivel gerenciar os produtos disponiveis e os seus dados.

De forma bastante simples e rápida é possível fazer a a inclusão, exclusão e edição de produtos. Também é possível fazer a  importação de mostruário de fábrica. 

Todas as inclusões, edições e exclusões de produtos ficam salvos em um arquivo .csv (estoque.csv) que é lido toda a vez que a aplicação é reiniciada.


## Funcionalidades ##
Ao iniciar a aplicação, será aberto um menu com as seguintes opções:


``` 
- - - MENU - - -

[1] Adicionar Novo Produto

[2] Editar Produto

[3] Excluir Produto

[4] Importar Mostruário da Fábrica

[5] Sair

Digite o número da opção desejada:
```


### [1] Adicionar Novo Produto
O usuário escolhe o nome do produto, preço, quantidade e categoria.
Para categoria é aberto um novo menu para que seja escolhida uma das categorias previamente cadastradas.
```
[1]ESCRITÓRIO  
[2]ALIMENTÇÃO E BEBIDAS  
[3]VESTUÁRIO  
[4]HIGIENE E SAÚDE  
[5]MEIO DE TRANSPORTE  
[6]OUTROS
Selecione a categoria:
```

O nome do produto aceita qualquer combinação de caracteres.

O preço aceita qualquer valor, com casa decimal ou não,que sejam positivos. Caso o usuário tente colocar um valor não numérico ou negativo, o sistema irá avisar e solicitar um novo valor.

Em quantidade é possível adicionar qualquer número inteiro entre 0 e 2147483647. Caso o usuário tente adicionar um valor com letras ou  fora do intervalo, o sistema irá avisar e solicitar um novo valor.

Para confirmar a operação o usuário é questionado pelo sistema.
```
Você quer confirmar a operação?
Digite [1] para confirmar ou [2] para cancelar: 
```
Novamente aqui foi feito tratamento de dados para que aceite apenas 1 ou 2 como resposta, retornando a informação `Dado inválido` caso seja escrito qualquer outro caracter.

Caso o usuário tente cadastrar um produto com o mesmo nome de  outro já salvo, o sistema irá apresentar a mensagem:
```
Produto com este nome já cadastrado. Utilize o menu Editar para modificar produto
```
E automaticamente abrirá o menu Editar Produto.

### [2] Editar Produto
O usuário também pode o selecionar esta opção no menu inicial. Com ela é demonstrado a lista de produtos já cadastrados.

O usuário é então demandado para escolher o produto a ser editado.
``Qual Id do produto?``

Caso digite um valor que não corresponda a nenhum produto, receberá a mensagem ``Digite apenas valores numéricos do Id do produto `` e será novamente questionado pelo id do produto que quer alterar.

Tendo escolhido um produto, é exposto os dados deste.
```
Produto escolhido
Id: 1, Nome: Grampeador, Preço: 12, Quantidade: 12, Categoria: ESCRITÓRIO
```

Em seguida um novo menu é aberto para que o usuário escolha o que quer modificar:
```
Qual dado quer modificar: [1]Nome [2]Preço [3]Quantidade [4]Categoria :
```
Novamente a aplicação só irá aceitar um dos caracteres do menu dando a mensagem `Dado inválido` caso se tente o contrário.

Após a escolha do que quer modificar, o sistema pede o dado atualizado do item  e em seguida mostra a modificação.e depois pergunta se quer confirmar a operação, assim como ao adicionar um novo produto.

```
Produto modificado 
Id: 1, Nome: Grampeador, Preço: 12, Quantidade: 12, Categoria: ESCRITÓRIO
```

Após é feita a confirmação: 

```
Você quer confirmar a operação?
Digite [1] para confirmar ou [2] para cancelar: 1
Operação realizada com sucesso
```

### [3] Excluir Produto

Ao selecionar essa opção, é aberta a lista dos produtos cadastrados.

O usuário seleciona o produto digitando o Id.
```
 Lista de produtos
1,Grampeador, 27.91,3, ESCRITÓRIO
7,Café Solúvel South,15.08,1,ALIMENTAÇÃO E BEBIDAS
8,Camiseta South,58.72,1,VESTUÁRIOAS
10,Máscara PFF2 South,15.26,1,HIGIENE E SAÚDE
11,Patinete Elétrico South,524.81,1,MEIO DE TRANSPORTE

Qual Id do produto? 1

Produto escolhido 
Id: 1, Nome: rfwe, Preço: 12, Quantidade: 12, Categoria: ESCRITÓRIO

Você quer confirmar a operação?
Digite [1] para confirmar ou [2] para cancelar: 1
Produto excluído
```

### [4] Importar Mostruário da Fábrica
Por essa opção do menu inicial é possivel importar um mostruário.
Toda vez que o usuário importar, será adicionado mais um item à quantidade caso o produto já tenha sido cadastrado anteriormente.

Em após a importação o sistema informa quantos produtos foram adicionados e quantos foram modificados, mostrando a lista atualizada na sequência.

```
Mostruário adicionado ao estoque.
Produtos adicionados: 2
Produtos modificados: 5
```

Caso o sistema não ache o arquivo de mostruário ou haja qualquer problema na importação, o sistema joga uma exceção e é feito o tratamento

### [5] Sair
Essa opção do menu inicial encerra o sistema.

### Observações
Foi feito o tratamento de dados digitados pelo usuário para que o sistema só aceite o que precisa para não quebrar, não tendo assim problemas com campos vazios, valores negativos, NullPointerException, ArrayOutOfBoundsException, InputMismatchException.


## PARA RODAR
Via IDE, usar o rodar classe Program na package aplication

Via terminal, chamar `java -jar desafio-backend-modulo1.jar` na pasta.